#!/bin/bash
source .env
USER_FROM_ENV="$USER"
PASSWORD_FROM_ENV="$PASSWORD"
docker build -t rabbitmq --build-arg USER=$USER_FROM_ENV --build-arg PASSWORD=$PASSWORD_FROM_ENV .
